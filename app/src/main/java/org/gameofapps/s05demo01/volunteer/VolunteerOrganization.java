package org.gameofapps.s05demo01.volunteer;

public class VolunteerOrganization {
    int logoDrawable;
    String name;
    boolean applied;
    int imageDrawable;
    String description;
    String details;

    VolunteerOrganization(
            int logoDrawable,
            String name,
            boolean applied,
            int imageDrawable,
            String description,
            String details) {
        this.logoDrawable = logoDrawable;
        this.name = name;
        this.imageDrawable = imageDrawable;
        this.description = description;
        this.details = details;
    }
}
