package org.gameofapps.s05demo01.volunteer;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<VolunteerOrganization> volunteerOrganizations = new ArrayList<>();
    int selectedVolunteerOrganizationIndex = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupData();
        setupScreen();
        setupCard1();
        setupCard2();
        setupCard3();
    }

    private void setupData() {
        VolunteerOrganization org1 = new VolunteerOrganization(
                R.drawable.logo1,
                "ABC Food Bank",
                false,
                R.drawable.organization01,
                "ABC Food Bank provides free meals anyone in need in the community.",
                "ABC Food Bank provides free meals anyone in need in the community.\n\nWe rely heavily on volunteers to help us achieve this.");
        VolunteerOrganization org2 = new VolunteerOrganization(
                R.drawable.logo2,
                "DEF Run for Health",
                false,
                R.drawable.organization02,
                "DEF Run for Health raises money for medical research.",
                "DEF Run for Health raises money for medical research.\n\nJoin our 10k run and support this meaningful cause.");
        VolunteerOrganization org3 = new VolunteerOrganization(
                R.drawable.logo3,
                "GHI Homes",
                false,
                R.drawable.organization03,
                "GHI Homes helps build homes for families in poverty-stricken locations worldwide.",
                "GHI Homes helps build homes for families in poverty-stricken locations worldwide.\n\nJoin us for a summer or support us with your donations.");
        volunteerOrganizations.add(org1);
        volunteerOrganizations.add(org2);
        volunteerOrganizations.add(org3);
    }

    private void setupScreen() {
        hideSignupScreen();
        hideConfirmationScreen();
        setupButtons();
    }

    private void hideSignupScreen() {
        View signUpScreenView = findViewById(R.id.signupBackground);
        signUpScreenView.setVisibility(View.GONE);
    }

    private void showSignupScreen() {
        // Check if something was selected
        if (selectedVolunteerOrganizationIndex < 0) {
            return;
        }

        // Check if selected index is valid
        if (volunteerOrganizations.size() <= selectedVolunteerOrganizationIndex) {
            return;
        }

        VolunteerOrganization org = volunteerOrganizations.get(selectedVolunteerOrganizationIndex);

        // Populate sign up screen
        TextView orgNameTextView = findViewById(R.id.signupOrgNameTextView);
        orgNameTextView.setText(org.name);
        ImageView orgImageView = findViewById(R.id.signupImageView);
        orgImageView.setImageResource(org.imageDrawable);
        TextView orgDetailsTextView = findViewById(R.id.signupDetailsTextView);
        orgDetailsTextView.setText(org.details);

        ScrollView scrollView = findViewById(R.id.signupScrollView);
        scrollView.fullScroll(ScrollView.FOCUS_UP);
        View signUpScreenView = findViewById(R.id.signupBackground);
        signUpScreenView.setVisibility(View.VISIBLE);

        clearSignUpScreenFields();
        clearSignUpScreenErrors();
    }

    private void clearSignUpScreenFields() {
        EditText fullNameEditText = findViewById(R.id.fullNameEditText);
        fullNameEditText.setText("");
        EditText emailAddressEditText = findViewById(R.id.emailAddressEditText);
        emailAddressEditText.setText("");
        EditText phoneNumberEditText = findViewById(R.id.phoneNumberEditText);
        phoneNumberEditText.setText("");
    }

    private void clearSignUpScreenErrors() {
        EditText fullNameEditText = findViewById(R.id.fullNameEditText);
        fullNameEditText.setBackground(getDrawable(R.drawable.rounded_edittext));
        TextView fullNameErrorTextView = findViewById(R.id.fullNameErrorTextView);
        fullNameErrorTextView.setVisibility(View.GONE);
        EditText emailAddressEditText = findViewById(R.id.emailAddressEditText);
        emailAddressEditText.setBackground(getDrawable(R.drawable.rounded_edittext));
        TextView emailAddressErrorTextView = findViewById(R.id.emailAddressErrorTextView);
        emailAddressErrorTextView.setVisibility(View.GONE);
        EditText phoneNumberEditText = findViewById(R.id.phoneNumberEditText);
        phoneNumberEditText.setBackground(getDrawable(R.drawable.rounded_edittext));
        TextView phoneNumberErrorTextView = findViewById(R.id.phoneNumberErrorTextView);
        phoneNumberErrorTextView.setVisibility(View.GONE);
    }

    private void hideConfirmationScreen() {
        View signUpScreenView = findViewById(R.id.confirmationBackground);
        signUpScreenView.setVisibility(View.GONE);
    }

    private void showConfirmationScreen() {
        View signUpScreenView = findViewById(R.id.confirmationBackground);
        signUpScreenView.setVisibility(View.VISIBLE);
    }

    private void setupButtons() {
        Button backButton = findViewById(R.id.backButton);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSignupScreen();
                selectedVolunteerOrganizationIndex = -1;
            }
        });
        Button submitButton = findViewById(R.id.submitButton);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                processSignup();
            }
        });
        Button doneButton = findViewById(R.id.doneButton);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideConfirmationScreen();
                selectedVolunteerOrganizationIndex = -1;
            }
        });
    }

    private void setupCard1() {
        // Check to make sure array contains at least one element
        if (volunteerOrganizations.size() < 1) { return; }

        VolunteerOrganization org1 = volunteerOrganizations.get(0);

        ImageView logoImageView = findViewById(R.id.org1LogoImageView);
        logoImageView.setImageResource(org1.logoDrawable);
        TextView orgNameTextView = findViewById(R.id.org1NameTextView);
        orgNameTextView.setText(org1.name);
        TextView haveAppliedTextView = findViewById(R.id.org1AppliedTextView);
        haveAppliedTextView.setText(org1.applied ? "Applied" : "Haven't applied");
        ImageView orgImageView = findViewById(R.id.org1ImageView);
        orgImageView.setImageResource(org1.imageDrawable);
        TextView orgDescriptionTextView = findViewById(R.id.org1DescriptionTextView);
        orgDescriptionTextView.setText(org1.description);
        Button orgViewInfoButton = findViewById(R.id.org1Button);
        orgViewInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Organization 1 details");
                selectedVolunteerOrganizationIndex = 0;
                showSignupScreen();
            }
        });
    }

    private void setupCard2() {
        // Check to make sure array contains at least two elements
        if (volunteerOrganizations.size() < 2) { return; }

        VolunteerOrganization org2 = volunteerOrganizations.get(1);

        ImageView logoImageView = findViewById(R.id.org2LogoImageView);
        logoImageView.setImageResource(org2.logoDrawable);
        TextView orgNameTextView = findViewById(R.id.org2NameTextView);
        orgNameTextView.setText(org2.name);
        TextView haveAppliedTextView = findViewById(R.id.org2AppliedTextView);
        haveAppliedTextView.setText(org2.applied ? "Applied" : "Haven't applied");
        ImageView orgImageView = findViewById(R.id.org2ImageView);
        orgImageView.setImageResource(org2.imageDrawable);
        TextView orgDescriptionTextView = findViewById(R.id.org2DescriptionTextView);
        orgDescriptionTextView.setText(org2.description);
        Button orgViewInfoButton = findViewById(R.id.org2Button);
        orgViewInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Organization 2 details");
                selectedVolunteerOrganizationIndex = 1;
                showSignupScreen();
            }
        });
    }

    private void setupCard3() {
        // Check to make sure array contains at least three elements
        if (volunteerOrganizations.size() < 3) { return; }

        VolunteerOrganization org3 = volunteerOrganizations.get(2);

        ImageView logoImageView = findViewById(R.id.org3LogoImageView);
        logoImageView.setImageResource(org3.logoDrawable);
        TextView orgNameTextView = findViewById(R.id.org3NameTextView);
        orgNameTextView.setText(org3.name);
        TextView haveAppliedTextView = findViewById(R.id.org3AppliedTextView);
        haveAppliedTextView.setText(org3.applied ? "Applied" : "Haven't applied");
        ImageView orgImageView = findViewById(R.id.org3ImageView);
        orgImageView.setImageResource(org3.imageDrawable);
        TextView orgDescriptionTextView = findViewById(R.id.org3DescriptionTextView);
        orgDescriptionTextView.setText(org3.description);
        Button orgViewInfoButton = findViewById(R.id.org3Button);
        orgViewInfoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("Organization 3 details");
                selectedVolunteerOrganizationIndex = 2;
                showSignupScreen();
            }
        });
    }

    private void updateAppliedOrganizations() {
        VolunteerOrganization org = volunteerOrganizations.get(0);
        TextView haveAppliedTextView = findViewById(R.id.org1AppliedTextView);
        haveAppliedTextView.setText(org.applied ? "Applied" : "Haven't applied");
        org = volunteerOrganizations.get(1);
        haveAppliedTextView = findViewById(R.id.org2AppliedTextView);
        haveAppliedTextView.setText(org.applied ? "Applied" : "Haven't applied");
        org = volunteerOrganizations.get(2);
        haveAppliedTextView = findViewById(R.id.org3AppliedTextView);
        haveAppliedTextView.setText(org.applied ? "Applied" : "Haven't applied");
    }

    private void processSignup() {
        boolean passedValidation = validateSignupEntries();
        if (!passedValidation) {
            return;
        }

        VolunteerOrganization org = volunteerOrganizations.get(selectedVolunteerOrganizationIndex);
        org.applied = true;
        selectedVolunteerOrganizationIndex = -1;
        updateAppliedOrganizations();

        hideSignupScreen();
        showConfirmationScreen();
    }

    private boolean validateSignupEntries() {
        clearSignUpScreenErrors();
        boolean passedValidation = true;

        EditText fullNameEditText = findViewById(R.id.fullNameEditText);
        TextView fullNameErrorTextView = findViewById(R.id.fullNameErrorTextView);
        String fullName = fullNameEditText.getText().toString();
        if (fullName.isEmpty()) {
            fullNameEditText.setBackground(getDrawable(R.drawable.rounded_orange_edittext));
            fullNameErrorTextView.setVisibility(TextView.VISIBLE);
            passedValidation = false;
        }

        EditText emailAddressEditText = findViewById(R.id.emailAddressEditText);
        TextView emailAddressErrorTextView = findViewById(R.id.emailAddressErrorTextView);
        String emailAddress = emailAddressEditText.getText().toString();
        if (emailAddress.isEmpty()) {
            emailAddressEditText.setBackground(getDrawable(R.drawable.rounded_orange_edittext));
            emailAddressErrorTextView.setVisibility(TextView.VISIBLE);
            passedValidation = false;
        }

        EditText phoneNumberEditText = findViewById(R.id.phoneNumberEditText);
        TextView phoneNumberErrorTextView = findViewById(R.id.phoneNumberErrorTextView);
        String phoneNumber = phoneNumberEditText.getText().toString();
        if (phoneNumber.isEmpty()) {
            phoneNumberEditText.setBackground(getDrawable(R.drawable.rounded_orange_edittext));
            phoneNumberErrorTextView.setVisibility(TextView.VISIBLE);
            passedValidation = false;
        }

        hideKeyboard();
        return passedValidation;
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(this);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        view.clearFocus();
    }
}
